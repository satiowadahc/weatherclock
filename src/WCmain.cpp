/*
 WCMain.cpp - Main Entry For Weather Clock
 Written by Chad Woitas
 Created January 10, 2020
*/


#include <WCtime.h>
#include <WCweather.h>
#include <WCBoard.h>
#include <WCsecure.h>

#include <WiFi.h>

uint32_t now = 0; // Loop Timer

// TODO implementation list
// Weather API
// Sun Time ARC
// Snow Height?
int offset = 0;
uint32_t offset_update = -20;
#define OFFSET_TIMER 1000

/*
* Main Entry Point
*/
void setup() {

  // TODO Make board initiliazer Function
  Serial.begin(115200);
  matrix.begin();
  delay(100);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
  }
  delay(100);
  configTime(UTC_6, 0, ntpServer);
  

  // Set up Display
  makeRain();
  makeClouds();
  drawTime();
  drawDate();
offset_update = millis();
  // Watch dog???
}


/*
* Process Loop
*/
void loop() {
  now = millis();
  if(now - cloud_update> CLOUD_TIMER){
    drawClouds();
    cloud_update = now;
  }
  if(now - rain_update > RAIN_TIMER){
    drawRain(rain_color.color);
    rain_update = now;
  }
  if(now - time_update > TIME_TIMER){
    drawDate();
    drawTime();
    time_update = now;
  }
  if(now - offset_update > OFFSET_TIMER){
    time_highlight   = adjust_brightness(time_highlight,   offset);
    time_color       = adjust_brightness(time_color,       offset);
    rain_color       = adjust_brightness(rain_color,       offset);
    rain_trail_color = adjust_brightness(rain_trail_color, offset);
    cloud_color      = adjust_brightness(cloud_color,      offset);

    offset_update = now;
  }


  delay(50); // Not a demanding process need to slow down the esp
}

