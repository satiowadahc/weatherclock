
/*
 WCBoard.cpp - Hardware Implentations and Universal variables for Weather Clock
 Written by Chad Woitas
 Created January 16, 2020
*/

#include <MATpins.h>
#include <WCBoard.h>

// Singular Access for matrix
RGBmatrixPanel matrix(ROW_A_pin, ROW_B_pin, ROW_C_pin, ROW_D_pin, SCR_CLK_pin, SCR_LAT_pin, SCR_OE_pin, false);


WCcolors setWCColor(uint8_t r, uint8_t g,uint8_t b){
    WCcolors temp;
    // Thought add defaults values and brightness setting
    temp.def_r = r;
    temp.def_g = g;
    temp.def_b = b;
    temp.r = r;
    temp.g = g;
    temp.b = b;
    temp.color = matrix.Color888(temp.r, temp.g, temp.b);
    return temp;
}

uint8_t offsetCalc(int c, int v){
    if       (c+v <= 0  ){return 0;}
    else if  (c+v >= 255){return 255;}
    else return c+v;
}

/*
* Value to adjust, maintains colors ratios
*/
WCcolors adjust_brightness(WCcolors temp, int value){
    temp.r = offsetCalc(temp.def_r, value);
    temp.g = offsetCalc(temp.def_g, value);
    temp.b = offsetCalc(temp.def_b, value);

    temp.color = matrix.Color888(temp.r, temp.g, temp.b);
    return temp;
}

WCcolors color_clear      = setWCColor(000, 000, 000);
WCcolors time_highlight   = setWCColor(067, 067, 067);
WCcolors time_color       = setWCColor(117, 025, 000);
WCcolors rain_color       = setWCColor(000, 000, 062);
WCcolors rain_trail_color = setWCColor(000, 000, 127);
WCcolors cloud_color      = setWCColor(067, 067, 067);
