/*
 WCweather.cpp - Weather Animations for Weather Clock
 Written by Chad Woitas
 Created January 16, 2020
*/

#include <WCweather.h>

uint32_t cloud_update = 0;   // Time Clouds Last Updated
uint32_t CLOUD_TIMER = 300;  // Time to redraw clouds
uint32_t rain_update = 0;    // Time Rain Last Updated
uint32_t RAIN_TIMER = 100;   // Time to redraw rain

cloud clouds[NUM_CLOUDS];    // Cloud Drawing Data
drops rain[NUM_DROPS];       // Rain Drawing Data

uint8_t wind_speed = 0;      // Affects direction of rain and clouds

/*
* Place Precipitation on Screen
*/
void makeRain(){
  for(auto& r: rain){
    r.xpos = random(0,31);
    r.ypos = RAIN_CEILING;
    r.delta_y = random(0,3);
  }
}

/*
* Animate Precipation on Screen
* Must call makeRain() before calling!
*/
void drawRain(uint16_t c){
  for(int i = 0; i<NUM_DROPS; i++){
    rain[i].x_trail = rain[i].xpos;
    rain[i].y_trail = rain[i].ypos;
    rain[i].ypos+=rain[i].delta_y;
    rain[i].xpos+=random(0,wind_speed+1);
    if(rain[i].ypos >=32){
      rain[i].ypos = RAIN_CEILING;
      rain[i].xpos = random(0,31);
      rain[i].delta_y = random(1,3);
    }
    //Draw first
    matrix.drawPixel(rain[i].xpos,   rain[i].ypos,   c);
    //Draw Tail
    matrix.drawPixel(rain[i].x_trail, rain[i].y_trail, rain_color.color);
    matrix.drawPixel(rain[i].x_last, rain[i].y_last, color_clear.color);
    rain[i].x_last = rain[i].x_trail;
    rain[i].y_last = rain[i].y_trail;

  }
}


/*
* Place Sun on Screen
*/
void drawSun(){ 
  matrix.drawCircle(8,22,3,matrix.Color888(127,127,0));
}

/*
* Place Clouds on Screen
*/
void makeClouds(){
  for(int i = 0; i < NUM_CLOUDS; i++){
    generateCloud(i);
  }
}

/*
* Helper Function for redrawing clouds 
*/
void generateCloud(int idx){
  clouds[idx].xpos_left = random(CLOUD_LEFT_LIM, 0);
  clouds[idx].ypos_left = random(CLOUD_FLOOR, CLOUD_CEILING);
  clouds[idx].height = random(2, 4);
  clouds[idx].width = random(4,10);
  clouds[idx].delta_x = random(1,wind_speed+2);
  clouds[idx].xpos_prev = 0;
  clouds[idx].ypos_prev = 0;
  uint8_t c = random(20,80);
  clouds[idx].color = matrix.Color888(c+random(-5,5),c+random(-5,5),c+random(-5,5));
}

/*
* Animate Clouds on Screen
*/
void drawClouds(){
  for(int i = 0; i < NUM_CLOUDS; i++){
    clouds[i].xpos_left += clouds[i].delta_x;
    if(clouds[i].xpos_left >= CLOUD_RIGHT_LIM){
      generateCloud(i);
    }

    switch (clouds[i].height){
    
    // First 3 cases are can cascade
    case 3:{ 
      matrix.drawFastHLine(clouds[i].xpos_left+1, clouds[i].ypos_left+1, clouds[i].width-2, clouds[i].color);
      matrix.drawFastHLine(clouds[i].xpos_prev+1, clouds[i].ypos_prev+1, clouds[i].delta_x, color_clear.color);
    }
    case 2:{ 
      matrix.drawFastHLine(clouds[i].xpos_left+1, clouds[i].ypos_left-1, clouds[i].width-2, clouds[i].color); 
      matrix.drawFastHLine(clouds[i].xpos_prev+1, clouds[i].ypos_prev-1, clouds[i].delta_x, color_clear.color);
    }
    case 1:{ 
      matrix.drawFastHLine(clouds[i].xpos_left,   clouds[i].ypos_left,   clouds[i].width,   clouds[i].color); 
      matrix.drawFastHLine(clouds[i].xpos_prev,   clouds[i].ypos_prev,   clouds[i].delta_x, color_clear.color);
      break;
    }

    default:
      // Something Weird happened, just remake the cloud
      generateCloud(i);
      break;
    }

    clouds[i].xpos_prev = clouds[i].xpos_left;
    clouds[i].ypos_prev = clouds[i].ypos_left;

  }

}