/*
 WCtime.cpp - Time Animations for Weather Clock
 Written by Chad Woitas
 Created January 16, 2020
*/

#include <WCtime.h>
#include <WCBoard.h>

#include <time.h>

uint32_t time_update = 0;

char days[7][3] = {{'S','u','n'},
                   {'M','o','n'},
                   {'T','u','e'},
                   {'W','e','d'},
                   {'T','h','r'},
                   {'F','r','i'},
                   {'S','a','t'} };

/*
* Place Day of Week and Month on Screen
*/
void drawDate(){
  struct tm timeinfo;
  getLocalTime(&timeinfo);
  DAY_IDX day = DAY_IDX(timeinfo.tm_wday);
  for(int i = 0; i<3; i++){
    matrix.drawChar((DATE_X_Origin + i*DATE_X_DELTA), DATE_Y_Origin, days[day][i], time_color.color,color_clear.color,1);
  }

  char dayt = (uint8_t(timeinfo.tm_mday / 10) % 10) + 48;
  matrix.drawChar((DATE_X_Origin + 3*DATE_X_DELTA + 1), DATE_Y_Origin, dayt, time_color.color,color_clear.color,1);
  dayt = (timeinfo.tm_mday % 10) + 48; 
  matrix.drawChar((DATE_X_Origin + 4*DATE_X_DELTA + 1), DATE_Y_Origin, dayt, time_color.color,color_clear.color,1);

}

/*
* Place Time of Day on Screen
*/
void drawTime(){
  struct tm timeinfo;

  getLocalTime(&timeinfo);

  // TODO Would writeLine Work Better?
  // Hours 
  if(timeinfo.tm_hour > 9){
    matrix.drawChar((TIME_X_Origin), TIME_Y_Origin, '1', time_color.color, color_clear.color,1);
  }  
  else if(timeinfo.tm_hour > 19){
    matrix.drawChar((TIME_X_Origin), TIME_Y_Origin, '2', time_color.color, color_clear.color,1);
  }
  else{
    matrix.fillRect((TIME_X_Origin), TIME_Y_Origin,TIME_X_DELTA,TIME_Y_Origin, color_clear.color);
  }

  char hr = (timeinfo.tm_hour % 10) + 48;
  matrix.drawChar((TIME_X_Origin + 1*TIME_X_DELTA), TIME_Y_Origin, hr, time_color.color, color_clear.color,1);

  // Colon
  matrix.drawPixel(14,15,time_highlight.color);
  matrix.drawPixel(14,13,time_highlight.color);
  
  // Minutes
  char min = (uint8_t(timeinfo.tm_min / 10) % 10) + 48; 
  matrix.drawChar((16               ), TIME_Y_Origin, min, time_color.color, color_clear.color,1);
  min = (timeinfo.tm_min % 10) + 48; 
  matrix.drawChar((16 + TIME_X_DELTA), TIME_Y_Origin, min, time_color.color, color_clear.color,1);
}

