/*
 WCweather.h - Weather Definions for Weather Clock
 Written by Chad Woitas
 Created January 16, 2020
*/

#ifndef __WC_WEATHER_H_
#define __WC_WEATHER_H_
#include <WCBoard.h>

/*
* Precipitation Related Variables
*/
#define NUM_DROPS 8
struct drops{
  uint8_t xpos = 0;
  uint8_t ypos = 0;
  uint16_t x_trail = 0;
  uint16_t y_trail = 0;
  uint8_t x_last = 0;
  uint8_t y_last = 0;
  uint8_t delta_y = 0;
};

#define RAIN_CEILING 24

/*
* Cloud Related Variables
* Weird for an IoT Clock? 
*/
#define NUM_CLOUDS       10
#define CLOUD_LEFT_LIM   (-32)
#define CLOUD_RIGHT_LIM  (35)
#define CLOUD_FLOOR      19
#define CLOUD_CEILING    23
struct cloud{
  uint8_t xpos_left = 0;
  uint8_t ypos_left = 0;
  uint8_t height = 0;
  uint8_t width = 0;
  uint8_t delta_x = 0;
  uint8_t xpos_prev = 0;
  uint8_t ypos_prev = 0;
  uint16_t color = 0;
};

extern uint8_t wind_speed;

extern uint32_t 
            cloud_update,
            CLOUD_TIMER,
            rain_update,
            RAIN_TIMER;

void
    makeRain(),
    drawRain(uint16_t),
    drawSun(),
    generateCloud(int),
    drawClouds(),
    makeClouds();

#endif