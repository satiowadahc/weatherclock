/*
 WCtime.h - Time Definitions for Weather Clock
 Written by Chad Woitas
 Created January 16, 2020
*/

#ifndef __WC_TIME_H
#define __WC_TIME_H

#include <WCBoard.h>

#define UTC_6 -21600
#define TIME_TIMER 750

/*
* Time Drawing Related Variables
*/ 
enum DAY_IDX{
  SUNDAY,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY
};

extern char days[7][3];

#define DATE_Y_Origin 1
#define DATE_X_Origin 1
#define DATE_X_DELTA  6

#define TIME_Y_Origin 10
#define TIME_X_Origin 2
#define TIME_X_DELTA  6

extern uint32_t time_update;

void
    drawDate(),
    drawTime();


#endif