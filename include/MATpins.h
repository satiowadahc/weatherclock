// Pinout for ESP32



#define R1_pin    16
#define G1_pin    17
#define B1_pin     5

#define R2_pin    13
#define G2_pin    12
#define B2_pin    14

#define ROW_A_pin 27
#define ROW_B_pin 26
#define ROW_C_pin 25
#define ROW_D_pin 33

#define SCR_CLK_pin 22
#define SCR_LAT_pin 18
#define SCR_OE_pin  21

