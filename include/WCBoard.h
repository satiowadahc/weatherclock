
/*
 WCBoard.h - Universal Definitions for Weather Clock
 Written by Chad Woitas
 Created January 16, 2020
*/


#ifndef __WC_BOARD_H_
#define __WC_BOARD_H_

#include <RGBmatrixPanel.h>
#include <Arduino.h>


extern RGBmatrixPanel matrix; 

struct WCcolors{
    int def_r;
    int def_g;
    int def_b;
    int r;
    int g;
    int b;
    uint16_t color;
};

/*
* Universal Drawing Related Variables
*/ 
extern WCcolors color_clear;
extern WCcolors time_highlight;
extern WCcolors time_color;
extern WCcolors rain_color;
extern WCcolors rain_trail_color;
extern WCcolors cloud_color;

WCcolors adjust_brightness(WCcolors, int);
#endif